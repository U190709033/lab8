package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    double height;
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }
    public double calculateArea() {
        return (2 * super.calculateArea()) + ((2 * Math.PI * super.getRadius()) * this.height);
    }
    public double calculateVolume() {
        return super.calculateArea() * this.height;
    }
    public void showInsValues() {
        System.out.println("Height and radius of the cylinder respectively are: " +
                this.height + ", " + super.getRadius());
    }
}
