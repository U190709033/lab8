package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube(double oneSideLength) {
        super(oneSideLength);
    }
    public double calculateArea() {
        return (6 * super.calculateArea());
    }
    public double calculateVolume() {
        return Math.pow(super.getOneSideLength(), 3);
    }
    public void showInsValues() {
        System.out.println("One side of the cube is: " + super.getOneSideLength());
    }
}
