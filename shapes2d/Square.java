package shapes2d;

public class Square {
    double oneSideLength;
    public Square(double oneSideLength) {
        this.oneSideLength = oneSideLength;
    }
    public double calculateArea() {
        return this.oneSideLength * this.oneSideLength;
    }
    public void showInsValues() {
        System.out.println("One side of the square is: " + oneSideLength);
    }
    public double getOneSideLength() {
        return oneSideLength;
    }
}
