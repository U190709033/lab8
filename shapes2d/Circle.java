package shapes2d;

public class Circle {
    double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double calculateArea() {
        return Math.PI * this.radius * this.radius;
    }
    public void showInsValues() {
        System.out.println("Radius of the circle is: " + radius);
    }
    public double getRadius() {
        return radius;
    }
}
