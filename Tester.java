import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cube;
import shapes3d.Cylinder;

public class Tester {
    public static void main(String[] args) {
        Circle myCircle = new Circle(2);
        System.out.println("Area of the circle: " + myCircle.calculateArea());
        myCircle.showInsValues();

        Square mySquare = new Square(2);
        System.out.println("Area of the square: " + mySquare.calculateArea());
        mySquare.showInsValues();

        Cube myCube = new Cube(2);
        System.out.println("Area of the cube: " + myCube.calculateArea());
        System.out.println("Volume of the cube : " + myCube.calculateVolume());
        myCube.showInsValues();

        Cylinder myCylinder = new Cylinder(2, 2);
        System.out.println("Area of the cylinder: " + myCylinder.calculateArea());
        System.out.println("Volume of the cylinder: " + myCylinder.calculateVolume());
        myCylinder.showInsValues();
    }
}
